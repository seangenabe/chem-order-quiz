export namespace GatsbyLifecycle {
  export interface CreatePages {
    (
      args: Readonly<{
        graphql<T = unknown>(
          query: string,
          context?: unknown
        ): Promise<{ data: T; errors?: unknown[] }>
        actions: Readonly<{
          readonly createPage: GatsbyActions.CreatePage
        }>
        getNode: any
      }>
    ): MaybePromise<void>
  }

  export interface CreatePagesStatefully extends CreatePages {}

  export interface GenerateSideEffects {
    (args: any): MaybePromise<void>
  }

  export interface OnCreateBabelConfig {
    (args: any): MaybePromise<void>
  }

  export interface OnCreateDevServer {
    (
      args: Readonly<{
        app: any // express
      }>
    ): MaybePromise<void>
  }

  export interface OnCreateNode {
    (
      args: Readonly<{
        node: GatsbyLokiNode
        getNode: any
        actions: Readonly<{
          createNode: GatsbyActions.CreateNode
          createNodeField: GatsbyActions.CreateNodeField
        }>
      }>
    ): MaybePromise<void>
  }

  export interface OnCreatePage {
    (
      args: Readonly<{
        actions: Readonly<{}>
      }>
    ): MaybePromise<void>
  }

  export interface OnCreateWebpackConfig {
    (
      args: Readonly<{
        stage: string
        getConfig(): unknown
        rules: unknown
        loaders: unknown
        plugins: unknown
        actions: Readonly<{
          setWebpackConfig: GatsbyActions.SetWebpackConfig
        }>
      }>
    ): MaybePromise<void>
  }

  export interface OnPostBootstrap {
    (args: {}): MaybePromise<void>
  }

  export interface OnPostBuild {
    (args: {}): MaybePromise<void>
  }

  export interface OnPreBootstrap {
    (args: {}): MaybePromise<void>
  }

  export interface OnPreBuild {
    (args: {}): MaybePromise<void>
  }

  export interface OnPreExtractQueries {
    (args: {}): MaybePromise<void>
  }

  export interface OnPreInit {
    (args: {}): MaybePromise<void>
  }

  export interface PreprocessSource {
    (args: {}): MaybePromise<void>
  }

  export interface ResolvableExtensions {
    (): string[]
  }

  export interface SetFieldsOnGraphQLNodeType {
    (args: {
      type: {
        name: string
        nodes: any
      }
    }): {}
  }

  export interface SourceNodes {
    (
      args: Readonly<{
        createNodeId(id: string): string
        createContentDigest(content: string): string
        actions: Readonly<{
          createNode: GatsbyActions.CreateNode
        }>
      }>
    ): MaybePromise<void>
  }
}

namespace GatsbyActions {
  export interface AddThirdPartySchema {
    (args: {
      schema: {} // GraphQLSchema
    }): void
  }

  export interface CreateJob {
    (job: { id: string }): void
  }

  export interface CreateNode {
    (node: GatsbyLokiNode, actionOptions: GatsbyActionOptions): void
  }

  export interface CreatePage {
    (
      args: {
        path: string
        component: string
        context?: unknown
      },
      actionOptions?: GatsbyActionOptions
    ): void
  }

  export interface CreateNodeField {
    (arg: { node: GatsbyLokiNode; name: string; value: unknown }): void
  }

  export interface SetWebpackConfig {
    (arg: {}): void
  }

  export interface CreateParentChildLink {
    (args: { parent: GatsbyLokiNode; child: GatsbyLokiNode }): void
  }
}

export interface GatsbyActionOptions {}

export interface GatsbyLokiNode {
  id: string
  parent?: GatsbyLokiNode
  children?: GatsbyLokiNode[]
  internal: {
    mediaType: string
    type: string
    content: string
    contentDigest: string
    description: string
  }
  fields?: { [key: string]: unknown }
}

type MaybePromise<T> = Promise<T> | T

export interface GatsbyFileNode extends GatsbyLokiNode {
  // name passed to instance of gatsby-source-filesystem
  sourceInstanceName: string
  // absolute path to file
  absolutePath: string
  // path to file relative to root
  relativePath: string
  // extension without dot
  extension: string
  size: number
  prettySize: string
  // root of the filesystem (C:/)
  root: string
  // parent directory of file
  dir: string
  // base filename - abc.md
  base: string
  // extension with dot - .md
  ext: string
  // file name - abc
  name: string
  // directory relative to gatsby-source-filesystem root
  relativeDirectory: string
  dev: number
  mode: number
  nlink: number
  uid: number
  gid: number
  rdev: number
  ino: number
  atimeMs: number
  mtimeMs: number
  ctimeMs: number
  birthtimeMs: number
  publicURL: string
}

export interface GatsbyMarkdownNode extends GatsbyLokiNode {
  frontmatter: {
    [key: string]: unknown
    title: string
    tags: string[]
    links: { source: string; href: string }[]
  }
  rawMarkdownBody: string
  fileAbsolutePath: string
  html: string
  htmlAst: unknown
  excerpt: string
  headings: MarkdownHeading[]
  timeToRead: number
  tableOfContents: string
  wordCount: WordCount
}

interface MarkdownHeading {
  value: string
  depth: number
}

interface WordCount {
  paragraphs: number
  sentences: number
  words: number
}
