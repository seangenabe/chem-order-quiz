import { plugins as postCssPlugins } from "./postcss.config"

export const siteMetadata = {
  title: "chem order quiz",
  description: "",
  shortTitle: "",
  author: "",
  keywords: []
}

export const plugins = [
  "gatsby-plugin-typescript",
  "gatsby-plugin-react-helmet",
  /*
  {
    resolve: "gatsby-source-filesystem",
    options: {
      name: "src",
      path: `${ROOT}/src`
    }
  },
  */
  /*
  {
    resolve: "gatsby-plugin-manifest",
    options: {
      name: siteMetadata.title,
      short_name: siteMetadata.shortTitle,
      start_url: "/",
      background_color: "#ffa0a0",
      theme_color: "#ffa0a0",
      display: "minimal-ui"
      //icon: "src/images/tailwind-icon.png"
    }
  },
  */
  {
    resolve: "gatsby-plugin-postcss",
    options: {
      postCssPlugins
    }
  }
  //"gatsby-transformer-remark"
  // https://gatsby.app/offline
  // , 'gatsby-plugin-offline'
]
