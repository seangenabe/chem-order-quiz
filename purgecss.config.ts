import { sync as globbySync } from "globby"

const ROOT = __dirname

const paths = globbySync(`**/*.{tsx,md}`, { cwd: `${ROOT}/src` })

const purgeCssConfig = {
  paths: paths.map((path: string) => `${ROOT}/src/${path}`),
  extractors: [
    {
      // Custom extractor to allow special characters (like ":") in class names
      // See: https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css-with-purgecss
      extractor: class {
        static extract(content: string) {
          return content.match(/[A-Za-z0-9-_:/]+/g) || []
        }
      },
      extensions: ["jsx", "js", "ts", "tsx", "md"]
    }
  ],
  whitelist: [],
  whitelistPatterns: [/body/]
}

export default purgeCssConfig
