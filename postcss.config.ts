import tailwind from "tailwindcss"
import precss from "precss"
import * as TAILWIND_CONFIG from "./tailwind.config"

export const plugins = [precss(), tailwind(TAILWIND_CONFIG)]
