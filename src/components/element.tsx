import * as React from "react"
import { elementsMap } from "../elements-list"

export function Element(props: {
  z: number
  hidden?: boolean
  className?: string
}) {
  const { z, hidden = false, className = "" } = props
  const element = elementsMap.get(z)
  if (!element) {
    return null
  }
  return (
    <div
      className={`flex flex-col text-center border border-grey p-4 ${className}`}
    >
      <div>{z}</div>
      <div className="text-4xl">{hidden ? "?" : element.symbol}</div>
      <div>{hidden ? "?" : element.name}</div>
    </div>
  )
}
