import * as React from "react"
import { Element } from "../components/element"
import randomInt from "random-int"

import "../styles/style.css"

export default class App extends React.Component<
  {},
  {
    z: number
    lower: number
    upper: number
    isAnswerVisible: boolean
    hint: number
  }
> {
  state = {
    z: randomInt(1, 118),
    lower: 1,
    upper: 118,
    isAnswerVisible: false,
    hint: 0
  }

  constructor(props) {
    super(props)
  }

  randomize() {
    this.setState(prev => ({
      z: randomInt(prev.lower, prev.upper),
      isAnswerVisible: false,
      hint: 0
    }))
  }

  reveal() {
    this.setState(prev => ({
      isAnswerVisible: true
    }))
  }

  hint() {
    this.setState(prev => ({
      hint: prev.hint + 1
    }))
  }

  render() {
    return (
      <main className="p-4 font-sans">
        <div className="flex justify-around">
          <div className={this.state.hint < 2 ? "hidden" : ""}>
            <Element z={this.state.z - 2} />
          </div>
          <Element z={this.state.z - 1} />
          <Element z={this.state.z} hidden={!this.state.isAnswerVisible} />
          <Element z={this.state.z + 1} />
          <div className={this.state.hint < 1 ? "hidden" : ""}>
            <Element z={this.state.z + 2} />
          </div>
        </div>
        <div className="my-4 flex">
          <button
            type="button"
            className="bg-orange hover:bg-orange-dark text-white px-4 py-2 flex-1"
            onClick={e => this.hint()}
          >
            Hint
          </button>
          <button
            type="button"
            className="bg-green hover:bg-green-dark text-white px-4 py-2 flex-1 ml-2"
            onClick={e => this.reveal()}
          >
            Reveal
          </button>
          <button
            type="button"
            className="bg-blue hover:bg-blue-dark text-white px-4 py-2 flex-1 ml-2"
            onClick={e => this.randomize()}
          >
            Next
          </button>
        </div>
        <div className="mt-4">
          <label className="block">
            <span className="">Lower bound:</span>
            <input
              className="border ml-2 w-12"
              type="number"
              value={this.state.lower}
              min="1"
              max={this.state.upper}
              onChange={e => {
                this.setLower(e.target.valueAsNumber)
                e.preventDefault()
              }}
            />
          </label>
          <label className="block mt-2">
            <span className="">Upper bound:</span>
            <input
              className="border ml-2 w-12"
              type="number"
              value={this.state.upper}
              min={this.state.lower}
              max="118"
              onChange={e => {
                this.setUpper(e.target.valueAsNumber)
                e.preventDefault()
              }}
            />
          </label>
        </div>
      </main>
    )
  }

  setLower(lower: number) {
    this.setState(() => ({ lower }))
  }

  setUpper(upper: number) {
    this.setState(() => ({ upper }))
  }
}
